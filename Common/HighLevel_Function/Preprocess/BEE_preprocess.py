from Common.utils.bee_utils.csc import RGB2YCbCr
from Common.utils.bee_utils.tensorops import pad,resizeTensor
from Common.utils.bee_utils.testfuncs import readPngToTorchIm
import cv2
import torch.nn as nn
import torch
import numpy as np


class BEE_Preprocess(nn.Module):
    def __init__(self):
        super().__init__()

    def RGB2YUV(self,imArrayRGB):
        imArray = RGB2YCbCr()(imArrayRGB)
        return imArray

    def ImageResample(self,x_org,header,device):
        h0, w0 = x_org.size(2), x_org.size(3)
        x = x_org if ([header.picture.picture_header.resized_size_h,header.picture.picture_header.resized_size_w] is None or (header.picture.picture_header.resized_size_h == h0 and header.picture.picture_header.resized_size_w == w0)) else resizeTensor(x_org, [header.picture.picture_header.resized_size_h,header.picture.picture_header.resized_size_w])
        x = x.to(device)
        return x

    def ImagePadding(self,x):
        p = 64  # maximum 6 strides of 2
        x = pad(x, p)
        return x

    def encode(self,img, header, device):
        x_org, _, _ = readPngToTorchIm(img)
        x_org.to(device)
        x_resample = self.ImageResample(x_org,header,device)
        x_pad = self.ImagePadding(x_resample)
        x_pad.to(device)
        imArray = self.RGB2YUV(x_pad)
        return [imArray, x_pad]