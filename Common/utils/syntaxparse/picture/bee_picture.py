import torch
from Common.utils.syntaxparse.utils import *
from Common.utils.bee_utils.testfuncs import construct_weights, readPngToTorchIm

class BEE_picture():
    def __init__(self):
        self.picture_header = BEE_picture_header()
        self.first_substream_data = None
        self.second_substream_data = None

    def write_header_to_stream(self, f):
        f = self.picture_header.write_header_to_stream(f)
        num_string = 2
        write_uints(f, (num_string,))
        write_uints(f, (len(self.first_substream_data[0]),))
        write_bytes(f, self.first_substream_data[0])
        write_uints(f, (len(self.second_substream_data[0]),))
        write_bytes(f, self.second_substream_data[0])

    def update(self, syntax, image, rate):
        imagename = image.split("/")[-1]
        x_org, _, _ = readPngToTorchIm(image)
        _,_,h,w = x_org.shape
        syntax = construct_weights(syntax, rate, imagename, h, w)
        self.picture_header.update(syntax, h, w)

    def read_header_from_stream(self, f):
        f = self.picture_header.read_header_from_stream(f)
        n_strings = read_uints(f, 1)[0]
        strings = []
        for _ in range(n_strings):
            s = read_bytes(f, read_uints(f, 1)[0])
            strings.append([s])
        self.first_substream_data = strings[0]
        self.second_substream_data = strings[1]

class BEE_picture_header():
    def __init__(self):
        self.model_id = None
        self.metric = None
        self.quality = None
        self.original_size_h = None
        self.original_size_w = None
        self.resized_size_h = None
        self.resized_size_w = None
        self.latent_code_shape_h = None
        self.latent_code_shape_w = None
        self.output_bit_depth = 8
        self.output_bit_shift = None
        self.double_precision_processing_flag = None
        self.deterministic_processing_flag = None
        self.fast_resize_flag = None
        self.reserved_5_bits = None
        self.mask_scale = mask_scale_header()
        self.num_first_level_tile = None
        self.num_second_level_tile = None
        self.adaptive_offset = adaptive_offset_header()
        self.num_wavefront_min = None
        self.num_wavefront_max = None
        self.waveshift = None
        # encoder only
        self.rate = None
        self.numIte = None

        self.postprocessing = False

    def write_header_to_stream(self, f):
        header = get_header(self.model_id, self.metric, self.quality)
        write_uchars(f, header)
        write_uints(f, (self.original_size_h, self.original_size_w))
        write_uints(f, (self.resized_size_h, self.resized_size_w))
        code = ((self.output_bit_depth - 1) << 4) | ((self.output_bit_shift)  & 0x0F)
        write_uchars(f,tuple([code]))
        aa = 1 if self.double_precision_processing_flag else 0
        bb = 1 if self.deterministic_processing_flag else 0
        cc = 1 if self.fast_resize_flag else 0
        dd = 1 if self.postprocessing else 0
        code = aa<<7 | bb<<6 | cc<<5 | dd << 4#dd is defatult 0 for postprocessing flag
        write_uchars(f,tuple([code]))
        f = self.mask_scale.write_header_to_stream(f)
        write_uchars(f, tuple([self.num_first_level_tile, self.num_second_level_tile]))
        f = self.adaptive_offset.write_header_to_stream(f)
        write_uchars(f,tuple([self.num_wavefront_min]))
        write_uchars(f,tuple([self.num_wavefront_max]))
        write_uchars(f,tuple([self.waveshift]))
        write_uints(f, (self.latent_code_shape_h, self.latent_code_shape_w))
        return f

    def update(self, syntax, h, w):
        self.model_id = 'quantyuv444-decoupled'
        self.metric = 'mse'
        self.quality = syntax['model_idx']
        self.original_size_h = h
        self.original_size_w = w
        self.resized_size_h = syntax['resized_size'][0]
        self.resized_size_w = syntax['resized_size'][1]
        # TODO: needs to be updated in forward function
        self.latent_code_shape_h = None
        self.latent_code_shape_w = None
        self.output_bit_depth = syntax['outputBitDepth']
        self.output_bit_shift = syntax['outputBitShift']
        self.double_precision_processing_flag = syntax['DoublePrecisionProcessing']
        self.deterministic_processing_flag = syntax['DeterminismSpeedup']
        self.fast_resize_flag = syntax['FastResize']
        self.reserved_5_bits = None
        self.mask_scale.update(syntax)
        self.num_first_level_tile = syntax['decSplit'][0]
        self.num_second_level_tile = syntax['decSplit'][1]
        self.adaptive_offset.update(syntax)
        self.num_wavefront_min = syntax['numthreads_min']
        self.num_wavefront_max = syntax['numthreads_max']
        self.waveshift = syntax['waveShift']
        # Encoder only
        self.rate = syntax['rate']
        self.numIte = syntax['numRefIte']

    def read_header_from_stream(self, f):
        self.model_id, self.metric, self.quality = parse_header(read_uchars(f, 3))
        self.original_size_h, self.original_size_w = read_uints(f, 2)
        self.resized_size_h, self.resized_size_w = read_uints(f, 2)
        code = read_uchars(f,1)[0]
        self.output_bit_depth = (code>>4) + 1
        self.output_bit_shift = code & 0x0F
        code = read_uchars(f,1)[0]
        self.double_precision_processing_flag = True if int((code & 0x80)>>7) else False
        self.deterministic_processing_flag = True if int((code & 0x40)>>6) else False
        self.fast_resize_flag = True if int((code & 0x20)>>5) else False
        self.postprocessing = True if int(((code & 0x20)>>4)& 1) else False#default 0

        self.reserved_5_bits = code & 0x1F
        f = self.mask_scale.read_header_from_stream(f)
        self.num_first_level_tile, self.num_second_level_tile = list(read_uchars(f,2))
        f = self.adaptive_offset.read_header_from_stream(f)
        self.num_wavefront_min = read_uchars(f,1)[0]
        self.num_wavefront_max = read_uchars(f,1)[0]
        self.waveshift = read_uchars(f,1)[0]
        self.latent_code_shape_h, self.latent_code_shape_w = read_uints(f, 2)
        return f

class mask_scale_header():
    def __init__(self):
        self.num_adaptive_quant_params = None
        self.num_block_based_skip_params = None
        self.num_latent_post_process_params = None
        self.filterList = None
    
    def write_header_to_stream(self, f):
        def writer (num, precise ):
            if precise:
                write_uints(f,[int(num*100000)])
            else:
                write_uchars(f,[int(num*100)])
        numfilters = [self.num_adaptive_quant_params, self.num_block_based_skip_params, self.num_latent_post_process_params]
        write_uchars(f,tuple(numfilters))
        for i in range(sum(numfilters)):
            filt2Write = self.filterList[i]
            code = ( filt2Write["mode"] << 4) |\
                ((1 if filt2Write["block_size"]>1 else 0)<<3)  & 0x0F |\
                ((1 if filt2Write["greater"] else 0)<<2)  & 0x0F |\
                ((1 if filt2Write["precise"][0] else 0)<<1)  & 0x0F |\
                ((1 if filt2Write["precise"][1] else 0))  & 0x0F

            write_uchars(f,tuple([code]))
            if filt2Write["block_size"] > 1:
                write_uchars(f,tuple([filt2Write["block_size"]]))
            writer(filt2Write["thr"],filt2Write["precise"][0])
            if filt2Write["mode"] == 5:
                writer(filt2Write["scale"][0],filt2Write["precise"][1])
                writer(filt2Write["scale"][1],filt2Write["precise"][1])
            else:
                writer(filt2Write["scale"][0],filt2Write["precise"][1])
            if filt2Write["mode"] == 4:
                write_uchars(f, tuple([len(filt2Write["channels"])]))
                if filt2Write["channels"]:
                    write_uchars(f, tuple(filt2Write["channels"]))
        return f

    def update(self, syntax):
        self.num_adaptive_quant_params, self.num_block_based_skip_params, self.num_latent_post_process_params = syntax['numfilters']
        self.filterList = syntax['filterCoeffs']

    def read_header_from_stream(self, f):
        weights = list(read_uchars(f,3))
        self.num_adaptive_quant_params, self.num_block_based_skip_params, self.num_latent_post_process_params = weights
        filter1 = []
        for i in range(sum(weights)):
            code = read_uchars(f,1)[0]
            blkSize = (code & 0x08) + 1
            greater = True if ((code & 0x04)) else False
            precise1 = True if ((code & 0x02)) else False
            precise2 = True if ((code & 0x01)) else False
            mode = code >> 4
            if blkSize > 1:
                blkSize = read_uchars(f,1)[0]
            thr = reader(f,precise1)
            b1 = reader(f,precise2)
            if mode == 5:
                b2 = reader(f,precise2)
                scale = [b1,b2]
            else:
                scale = [b1]
            channel_num = []
            if mode == 4:
                numFilters = read_uchars(f,1)[0]
                if numFilters > 0:
                    channel_num = list(read_uchars(f,numFilters))
            filter = {"thr":thr,"scale":scale,"greater":greater,"mode":mode,"block_size":blkSize,"channels":channel_num}
            filter1.append(filter)
            self.filterList = filter1
        return f
    
class adaptive_offset_header():
    def __init__(self):
        self.adaptive_offset_enabled_flag = None
        self.reserved_7_bit = None
        self.num_horizontal_split = None
        self.num_vertical_split = None
        self.numChannels4offset = 0
        self.offsetPrecision = None
        self.offset_signalled = None
        self.offsetList = None
    
    def write_header_to_stream(self, f):
        # TODO remove first line
        #self.offsetList = torch.ones((self.num_vertical_split, self.num_horizontal_split, 192))
        write_uchars(f,tuple([1 if self.adaptive_offset_enabled_flag else 0]))
        if self.adaptive_offset_enabled_flag:
            write_uchars(f,tuple([self.num_horizontal_split,self.num_vertical_split]))
            write_uints(f,tuple([self.offsetPrecision]))
            means_full = self.offsetList
            dummy = means_full*self.offsetPrecision **2
            
            nonZeroChannels = torch.sum(torch.sum(torch.abs((dummy).round()),dim=0),dim=0)
            
            _,indices = torch.sort(nonZeroChannels,descending=True)
            nonZeroChannels = nonZeroChannels.to("cpu")
            indices = indices.to("cpu")
            means_full = (means_full*self.offsetPrecision).round()
            means_full = means_full.to("cpu")
            for ind,val in enumerate(indices):
                if ind > self.numChannels4offset:
                    nonZeroChannels[val] = 0
            bytestream = []
            for i in range(0,192,8):
                byte = (0 if nonZeroChannels[i] == 0 else 1)<<7 |\
                    (0 if nonZeroChannels[i+1] == 0 else 1)<<6 |\
                    (0 if nonZeroChannels[i+2] == 0 else 1)<<5 |\
                    (0 if nonZeroChannels[i+3] == 0 else 1)<<4 |\
                    (0 if nonZeroChannels[i+4] == 0 else 1)<<3 |\
                    (0 if nonZeroChannels[i+5] == 0 else 1)<<2 |\
                    (0 if nonZeroChannels[i+6] == 0 else 1)<<1 |\
                    (0 if nonZeroChannels[i+7] == 0 else 1)
                bytestream.append(byte)
            for x in range(self.num_vertical_split):
                for y in range(self.num_horizontal_split):
                    means = means_full[x,y,:]
                    for i in range(192):
                        if not nonZeroChannels[i] == 0:
                            sign = 1 if means[i]<0 else 0
                            val = int(abs(means[i])) if int(abs(means[i])) < 127 else 127
                            byte = (sign << 7) | (val & 0x7F)
                            bytestream.append(byte)
            write_uchars(f,tuple(bytestream))
        return f
    
    def update(self, syntax):
        self.adaptive_offset_enabled_flag = syntax['channelOffsetTool']
        self.num_horizontal_split = syntax['offsetSplit_w']
        self.num_vertical_split = syntax['offsetSplit_h']
        self.offsetPrecision = syntax['offsetPrecision']
        self.offset_signalled = None
        self.numChannels4offset = syntax['numChannels4offset']
        # TODO initialized in forward function.
        self.offsetList = None

    def read_header_from_stream(self, f):
        code = read_uchars(f,1)[0]
        self.adaptive_offset_enabled_flag = True if code else 0
        self.reserved_7_bit = (code & 0xFE) >> 1
        if self.adaptive_offset_enabled_flag:
            self.num_horizontal_split = list(read_uchars(f,1))[0]
            self.num_vertical_split = list(read_uchars(f,1))[0]
            self.offsetPrecision = read_uints (f,1)[0]
            means_full = torch.zeros((self.num_vertical_split, self.num_horizontal_split, 192))
            nonZeroChannels = []
            for i in range(0,192,8):
                byte = read_uchars(f,1)[0]
                nonZeroChannels.append((byte & 0x80)>>7)
                nonZeroChannels.append((byte & 0x40)>>6)
                nonZeroChannels.append((byte & 0x20)>>5)
                nonZeroChannels.append((byte & 0x10)>>4)
                nonZeroChannels.append((byte & 0x08)>>3)
                nonZeroChannels.append((byte & 0x04)>>2)
                nonZeroChannels.append((byte & 0x02)>>1)
                nonZeroChannels.append(byte & 0x01)
            self.offset_signalled = nonZeroChannels
            for x in range(self.num_vertical_split): 
                for y in range (self.num_horizontal_split):
                    means = torch.zeros(192)
                    for i in range(0,192):
                        if nonZeroChannels[i] >0:
                            byte = read_uchars(f,1)[0]
                            sign = -1 if ((byte & 0x80)>>7) else 1
                            val = (byte & 0x7F)
                            means[i] = sign*val
                    means_full[x,y,:]=means
            self.offsetList = means_full.permute(2,0,1)/self.offsetPrecision
        return f