# BEE Training Guidelines

The software is the reference software for IEEE 1857.11 standard for neural network-based image coding, including training functionalities.

Reference software is useful in aiding users of a video coding standard to establish and test conformance and interoperability, and to educate users and demonstrate the capabilities of the standard. For these purposes, this software is provided as an aid for the study and implementation of Neural Network-Based Image Coding.

The software has been developed by the IEEE 1857.11 Working Group.

## Training

### Prepare the data
Convert the png files to lmdb data by running script `Train/BEE_Train/prep_lmdb.py`.

You probably also need to convert validation dataset to lmdb format.

### Start training

Please modify the configuration files under `Train/BEE_Train/cfg`, especially the `trainpath` and `evalpath` to your local path.

It is suggested, but not required, to use 2 GPUs for each model to speedup the training progress.

The full training includes 3 stages.
- Stage 1: train models with QP = {2, 4, 6, 8, 10}.
- Stage 2: finetune models from Stage 1.
- Stage 3: finetune to obtain models with QP = {1, 3, 5, 7, 9, 11-16}.

Final 16 models are from Stage 2 and 3.

> To accelerate the development progress, it is feasible to only train Stage 1 and 2. Stage 3 is used to support rate adaptation.

**Example Stage 1:**
```bash
bash Train/BEE_Train/trainYUV.sh \
        -c Train/BEE_Train/cfg/TrainConfigStage1 \
        --quality 2 \
        --checkpoint logs/Stage1/Q2
```

**Example Stage 2:** \
Use corresponding models from Stage 1 to initialize, _i.e.,_ `logs/Stage1/Q{quality}/best.pth` \
```bash
bash Train/BEE_Train/trainYUV.sh \
        -c Train/BEE_Train/cfg/TrainConfigStage2 \
        --quality 2 \
        --checkpoint logs/Stage2/Q2 \
        --InitModel logs/Stage1/Q2/best.pth
```

**Example Stage 3:** \
When --quality in {1, 3, 5, 7, 9} use `logs/Stage2/Q{quality+1}/best.pth` to initialize \
When --quality in {10-16}, always use `logs/Stage2/Q10/best.pth` to initialize 
```bash
bash Train/BEE_Train/trainYUV.sh \
        -c Train/BEE_Train/cfg/TrainConfigStage3 \
        --quality 1 \
        --checkpoint logs/Stage3/Q1 \
        --InitModel logs/Stage2/Q2/best.pth
```





